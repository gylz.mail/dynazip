package local.dyncompr.dynazip.persistence.model;

import java.util.Date;

import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Draft - For showing an idea only...
 * @author Guillermo Fernandez
 */
@Document("compressed_data")
public class CompressedData {
	
	@Id
	private String id = null;
	
	@NotNull
	@Indexed(name = "fileName_index", direction = IndexDirection.ASCENDING, unique = true)
	private String fileName = null;
	
	private byte[] content = null;
	
	private int compressedMethod = 0;
	
	private long crc32 = -1;
	
	private int uncompressedSize = 0;
	
	private int compressedSize = 0;
	
	@Indexed(name = "group_index", direction = IndexDirection.ASCENDING, unique = false)
	private String group = null;
	
	@NotNull
	private Date creationDateTime = null;
	
	private Date updateDateTime = null;
	
	@NotNull
	private String creationProgram = null;
	
	private String updateProgram = null;
	
	public CompressedData() {
		// NOTHING TO DO
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public byte[] getContent() {
		return content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}

	public int getCompressedMethod() {
		return compressedMethod;
	}

	public void setCompressedMethod(int compressedMethod) {
		this.compressedMethod = compressedMethod;
	}

	public long getCrc32() {
		return crc32;
	}

	public void setCrc32(long crc32) {
		this.crc32 = crc32;
	}

	public int getUncompressedSize() {
		return uncompressedSize;
	}

	public void setUncompressedSize(int uncompressedSize) {
		this.uncompressedSize = uncompressedSize;
	}

	public int getCompressedSize() {
		return compressedSize;
	}

	public void setCompressedSize(int compressedSize) {
		this.compressedSize = compressedSize;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public Date getCreationDateTime() {
		return creationDateTime;
	}

	public void setCreationDateTime(Date creationDateTime) {
		this.creationDateTime = creationDateTime;
	}

	public Date getUpdateDateTime() {
		return updateDateTime;
	}

	public void setUpdateDateTime(Date updateDateTime) {
		this.updateDateTime = updateDateTime;
	}

	public String getCreationProgram() {
		return creationProgram;
	}

	public void setCreationProgram(String creationProgram) {
		this.creationProgram = creationProgram;
	}

	public String getUpdateProgram() {
		return updateProgram;
	}

	public void setUpdateProgram(String updateProgram) {
		this.updateProgram = updateProgram;
	}
	
}
