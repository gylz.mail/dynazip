package local.dyncompr.dynazip.persistence.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import local.dyncompr.dynazip.persistence.model.CompressedData;

/**
 * Draft - For showing an idea only...
 * @author Guillermo Fernandez
 */
@Repository
public interface CompressedDataRepository extends MongoRepository<CompressedData, String>, CompressedDataRepositoryCustom {
	
	@Query(value="{}", fields="{fileName: 1, _id: 0}")
	List<CompressedData> findAllFileNames();
	
	CompressedData findByFileName(String fileName);
	
	List<CompressedData> findByFileNameIn(List<String> fileNames);
	
}
