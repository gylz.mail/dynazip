package local.dyncompr.dynazip.util;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Draft - For showing an idea only...
 * @author Guillermo Fernandez
 */
public class DOSDateTimeOps {
	
	private DOSDateTimeOps() {
		// NOTHING TO DO
	}
	
	public static byte[] getDOSDateAndTime(Date date) {
		byte[] result = new byte[4];
		long value = javaToDosTime(date);
		for (int i = 0; i < 4; ++i) {
	        result[i] = (byte)(value & 0xFF);
	        value >>>= 8;
	    }
		return result;
	}
	
	static final long DOSTIME_BEFORE_1980 = (1 << 21) | (1 << 16);
	
	private static long javaToDosTime(Date date) {
		long result = DOSTIME_BEFORE_1980;
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(date);
        int year = calendar.get(Calendar.YEAR);
        if (year >= 1980) {
        	result = (year - 1980) << 25
        			| (calendar.get(Calendar.MONTH) + 1) << 21
        			| calendar.get(Calendar.DAY_OF_MONTH) << 16
        			| calendar.get(Calendar.HOUR_OF_DAY) << 11
        			| calendar.get(Calendar.MINUTE) << 5
        			| calendar.get(Calendar.SECOND) >> 1;
        	result += ((date.getTime() % 2000) << 32);
        }
        return result;
    }
	
}
