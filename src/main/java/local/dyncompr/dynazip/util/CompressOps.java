package local.dyncompr.dynazip.util;

import java.util.Arrays;
import java.util.zip.CRC32;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

/**
 * Draft - For showing an idea only...
 * @author Guillermo Fernandez
 */
public final class CompressOps {
	
	private CompressOps() {
		// NOTHING TO DO
	}
	
	public static byte[] deflate(byte[] bytes) {
		Deflater deflater = new Deflater(Deflater.DEFAULT_COMPRESSION, true);
		byte[] deflatedBytes = new byte[bytes.length + 200];
		deflater.setInput(bytes);
		deflater.finish();
		deflater.deflate(deflatedBytes);
		int deflatedSize = (int)(deflater.getBytesWritten());
		return Arrays.copyOfRange(deflatedBytes, 0, deflatedSize);
	}
	
	public static byte[] inflate(byte[] bytes, int compressedSize, int uncompressedSize) throws DataFormatException {
		byte[] result = null;
		Inflater inflater = new Inflater(true);
		inflater.setInput(bytes, 0, compressedSize);
		result = new byte[uncompressedSize];
		inflater.inflate(result);
		inflater.end();
		return result;
	}
	
	public static long crc32(byte[] bytes) {
		CRC32 crc = new CRC32();
		crc.update(bytes, 0, bytes.length);
		return crc.getValue();
	}
	
	public static byte[] crc32ToZipFileArray(long value) {
		byte[] result = new byte[4];
		for (int i = 0; i < 4; ++i) {
	        result[i] = (byte)(value & 0xFF);
	        value >>>= 8;
	    }
		return result;
	}

}
