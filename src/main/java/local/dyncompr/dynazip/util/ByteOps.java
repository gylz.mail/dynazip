package local.dyncompr.dynazip.util;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;

/**
 * Draft - For showing an idea only...
 * @author Guillermo Fernandez
 */
public final class ByteOps {
	
	private ByteOps() {
		// NOTHING TO DO
	}

	public static byte[] fromHexToByte(char[] chars) {
		byte[] result = new byte[chars.length / 2];
		int i = 0;
		int j = 0;
		while(i < chars.length) {
			int first = Character.digit(chars[i++], 16);
			int second = Character.digit(chars[i++], 16);
			if(first == -1 || second == -1) {
				throw new IllegalArgumentException("Invalid hexadecimal character in: " + new String(chars));
			}
			result[j++] = (byte)((first << 4) + second);
		}
		return result;
	}
	
	public static boolean match(byte[] pattern, byte[] bytes, int offset) {
		boolean result = true;
		for(int i = offset, j = 0; i < bytes.length && j < pattern.length; ++i, ++j) {
			if(bytes[i] != pattern[j]) {
				result = false;
				break;
			}
		}
		return result;
	}
	
	public static int sumBytesLittleEnddian(byte[] bytes, int offset, int length) {
		int result = 0;
		for(int i = (offset + length - 1); i >= offset; --i) {
			result = (result << 8) + Byte.toUnsignedInt(bytes[i]);
		}
		return result;
	}
	
	// LONG NUMBERS METHODS...
	public static byte[] toBytesBigEnddian(long value) {
	    byte[] result = new byte[8];
	    for (int i = 7; i >= 0; --i) {
	        result[i] = (byte)(value & 0xFF);
	        value >>= 8;
	    }
	    return result;
	}
	
	public static byte[] toBytesLittleEnddian(long value) {
	    byte[] result = new byte[8];
	    for (int i = 0; i < 8; ++i) {
	        result[i] = (byte)(value & 0xFF);
	        value >>= 8;
	    }
	    return result;
	}
	
	public static int writeBigEnddian(OutputStream out, long value) throws IOException {
		int last = (int)(value & 0xFFFFFFFF);
		value >>= 8;
		out.write((int)(value & 0xFFFFFFFF));
		out.write(last);
		return 8;
	}
	
	public static int writeLittleEnddian(OutputStream out, long value) throws IOException {
	    for (int i = 0; i < 8; ++i) {
	        out.write((byte)(value & 0xFF));
	        value >>= 8;
	    }
	    return 8;
	}
	
	public static long toLongFromBigEnddian(byte[] b) {
	    long result = 0;
	    for (int i = 0; i < 8; ++i) {
	        result <<= 8;
	        result |= (b[i] & 0xFF);
	    }
	    return result;
	}
	
	public static long toLongFromLittleEnddian(byte[] b) {
	    long result = 0;
	    for (int i = 7; i >= 0; --i) {
	        result <<= 8;
	        result |= (b[i] & 0xFF);
	    }
	    return result;
	}
	
	
	// INT NUMBERS METHODS...
	public static byte[] toBytesBigEnddian(int value) {
	    byte[] result = new byte[4];
	    for (int i = 3; i >= 0; --i) {
	        result[i] = (byte)(value & 0xFF);
	        value >>= 8;
	    }
	    return result;
	}
	
	public static byte[] toBytesLittleEnddian(int value) {
	    byte[] result = new byte[4];
	    for (int i = 0; i < 4; ++i) {
	        result[i] = (byte)(value & 0xFF);
	        value >>= 8;
	    }
	    return result;
	}
	
	public static int writeBigEnddian(OutputStream out, int value) throws IOException {
		out.write(value);
		return 4;
	}
	
	public static int writeLittleEnddian(OutputStream out, int value) throws IOException {
	    for (int i = 0; i < 4; ++i) {
	        out.write((byte)(value & 0xFF));
	        value >>= 8;
	    }
	    return 4;
	}

	public static int toIntFromBigEnddian(byte[] b) {
	    int result = 0;
	    for (int i = 0; i < 4; ++i) {
	        result <<= 8;
	        result |= (b[i] & 0xFF);
	    }
	    return result;
	}
	
	public static int toIntLittleBigEnddian(byte[] b) {
	    int result = 0;
	    for (int i = 3; i >= 0; --i) {
	        result <<= 8;
	        result |= (b[i] & 0xFF);
	    }
	    return result;
	}
	
	// SHORT NUMBERS METHODS...
	public static byte[] toBytesBigEnddian(short value) {
	    byte[] result = new byte[2];
	    for (int i = 1; i >= 0; --i) {
	        result[i] = (byte)(value & 0xFF);
	        value >>= 8;
	    }
	    return result;
	}
	
	public static byte[] toBytesLittleEnddian(short value) {
	    byte[] result = new byte[2];
	    for (int i = 0; i < 2; ++i) {
	        result[i] = (byte)(value & 0xFF);
	        value >>= 8;
	    }
	    return result;
	}
	
	public static int writeBigEnddian(OutputStream out, short value) throws IOException {
		out.write(value);
		return 2;
	}
	
	public static int writeLittleEnddian(OutputStream out, short value) throws IOException {
	    for (int i = 0; i < 2; ++i) {
	        out.write((byte)(value & 0xFF));
	        value >>= 8;
	    }
	    return 2;
	}

	public static short toShortFromBigEnddian(byte[] b) {
	    short result = 0;
	    for (int i = 0; i < 2; ++i) {
	        result <<= 8;
	        result |= (b[i] & 0xFF);
	    }
	    return result;
	}
	
	public static short toShortFromLittleEnddian(byte[] b) {
	    short result = 0;
	    for (int i = 1; i >= 0; --i) {
	        result <<= 8;
	        result |= (b[i] & 0xFF);
	    }
	    return result;
	}
	
	// ARRAYS METHODS..
	public static int writeBytes(OutputStream out, byte[] bytes) throws IOException {
		out.write(bytes);
		return bytes.length;
	}
	
	public static int writeBigEnddianFromBigEnddianArray(OutputStream out, byte[] bytes) throws IOException {
		out.write(bytes);
		return bytes.length;
	}
	
	public static int writeBigEnddianFromLittleEnddianArray(OutputStream out, byte[] bytes) throws IOException {
		for (int i = bytes.length - 1; i >= 0; --i) {
	    	out.write(bytes[i]);
	    }
		return bytes.length;
	}
	
	public static int writeLittleEnddianFromLittleEnddianArray(OutputStream out, byte[] bytes) throws IOException {
	    out.write(bytes);
	    return bytes.length;
	}
	
	public static int writeLittleEnddianFromBigEnddianArray(OutputStream out, byte[] bytes) throws IOException {
	    for (int i = bytes.length - 1; i >= 0; --i) {
	    	out.write(bytes[i]);
	    }
	    return bytes.length;
	}
	
	// STRING METHODS
	public static int write(OutputStream out, String value, Charset charset) throws IOException {
		byte[] bytes = charset.encode(value).array();
		out.write(bytes);
		return bytes.length;
	}
	
	public static int writeString(OutputStream out, String value, Charset charset) throws IOException {
		byte[] bytes = charset.encode(value).array();
		int i = 0;
		byte b;
		while(i < bytes.length && (b = bytes[i]) != 0) {
			out.write(b);
			++i;
		}
		return i;
	}
	
}
