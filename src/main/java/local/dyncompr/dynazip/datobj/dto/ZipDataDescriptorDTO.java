package local.dyncompr.dynazip.datobj.dto;

import java.util.Map;

/**
 * Draft - For showing an idea only...
 * @author Guillermo Fernandez
 */
public class ZipDataDescriptorDTO {
	
	private int offset = -1;
	
	private int length = -1;
	
	private Map<String, Object> descriptor = null;
	
	public ZipDataDescriptorDTO() {
		// NOTHING TO DO
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public Map<String, Object> getDescriptor() {
		return descriptor;
	}

	public void setDescriptor(Map<String, Object> descriptor) {
		this.descriptor = descriptor;
	}

}
