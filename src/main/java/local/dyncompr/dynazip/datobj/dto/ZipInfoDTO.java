package local.dyncompr.dynazip.datobj.dto;

import java.util.List;
import java.util.Map;

/**
 * Draft - For showing an idea only...
 * @author Guillermo Fernandez
 */
public class ZipInfoDTO {
	
	private byte[] bytes = null;
	
	private String hexPrettyBytes = null;
	
	private Map<String, List<Integer>> offsets = null;
	
	private Map<String, Object> endOfCentralDirectoryRecord = null;
	
	private Integer endOfCentralDirectoryRecordOffset = null;
	
	private Integer endOfCentralDirectoryRecordLength = null;
	
	private List<ZipCentralDirectoryFileHeaderDTO> centralDirectoryFileHeaders = null;
	
	private List<ZipLocalFileHeaderDTO> localFileHeaders = null;
	
	private List<ZipDataDescriptorDTO> dataDescriptors = null;
	
	private List<ZipDataDTO> data = null;
	
	private boolean errorDetected = false;
	
	private String devMsg = null;
	
	public ZipInfoDTO() {
		// NOTHING TO DO
	}

	public byte[] getBytes() {
		return bytes;
	}

	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}

	public String getHexPrettyBytes() {
		return hexPrettyBytes;
	}

	public void setHexPrettyBytes(String hexPrettyBytes) {
		this.hexPrettyBytes = hexPrettyBytes;
	}

	public Map<String, List<Integer>> getOffsets() {
		return offsets;
	}

	public void setOffsets(Map<String, List<Integer>> offsets) {
		this.offsets = offsets;
	}

	public Map<String, Object> getEndOfCentralDirectoryRecord() {
		return endOfCentralDirectoryRecord;
	}

	public void setEndOfCentralDirectoryRecord(Map<String, Object> endOfCentralDirectoryRecord) {
		this.endOfCentralDirectoryRecord = endOfCentralDirectoryRecord;
	}

	public Integer getEndOfCentralDirectoryRecordOffset() {
		return endOfCentralDirectoryRecordOffset;
	}

	public void setEndOfCentralDirectoryRecordOffset(Integer endOfCentralDirectoryRecordOffset) {
		this.endOfCentralDirectoryRecordOffset = endOfCentralDirectoryRecordOffset;
	}

	public Integer getEndOfCentralDirectoryRecordLength() {
		return endOfCentralDirectoryRecordLength;
	}

	public void setEndOfCentralDirectoryRecordLength(Integer endOfCentralDirectoryRecordLength) {
		this.endOfCentralDirectoryRecordLength = endOfCentralDirectoryRecordLength;
	}

	public List<ZipCentralDirectoryFileHeaderDTO> getCentralDirectoryFileHeaders() {
		return centralDirectoryFileHeaders;
	}

	public void setCentralDirectoryFileHeaders(List<ZipCentralDirectoryFileHeaderDTO> centralDirectoryFileHeaders) {
		this.centralDirectoryFileHeaders = centralDirectoryFileHeaders;
	}

	public List<ZipLocalFileHeaderDTO> getLocalFileHeaders() {
		return localFileHeaders;
	}

	public void setLocalFileHeaders(List<ZipLocalFileHeaderDTO> localFileHeaders) {
		this.localFileHeaders = localFileHeaders;
	}

	public List<ZipDataDescriptorDTO> getDataDescriptors() {
		return dataDescriptors;
	}

	public void setDataDescriptors(List<ZipDataDescriptorDTO> dataDescriptors) {
		this.dataDescriptors = dataDescriptors;
	}

	public List<ZipDataDTO> getData() {
		return data;
	}

	public void setData(List<ZipDataDTO> data) {
		this.data = data;
	}

	public boolean getErrorDetected() {
		return errorDetected;
	}

	public void setErrorDetected(boolean errorDetected) {
		this.errorDetected = errorDetected;
	}

	public String getDevMsg() {
		return devMsg;
	}

	public void setDevMsg(String devMsg) {
		this.devMsg = devMsg;
	}
	
}
