package local.dyncompr.dynazip.datobj.dto;

/**
 * Draft - For showing an idea only...
 * @author Guillermo Fernandez
 */
public class ZipDataDTO {
	
private String fileName = null;
	
	private int offset = -1;
	
	private int length = -1;
	
	private byte[] content = null;
	
	private String uncompressedContent = null;
	
	public ZipDataDTO() {
		// NOTHING TO DO
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public byte[] getContent() {
		return content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}

	public String getUncompressedContent() {
		return uncompressedContent;
	}

	public void setUncompressedContent(String uncompressedContent) {
		this.uncompressedContent = uncompressedContent;
	}

}
