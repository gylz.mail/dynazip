package local.dyncompr.dynazip.datobj.dto;

import java.util.List;

/**
 * Draft - For showing an idea only...
 * @author Guillermo Fernandez
 */
public class CompDataPersistedFilesNamesDTO {
	
	private List<String> names = null;
	
	public CompDataPersistedFilesNamesDTO() {
		// NOTHING TO DO
	}

	public List<String> getNames() {
		return names;
	}

	public void setNames(List<String> names) {
		this.names = names;
	}

}
