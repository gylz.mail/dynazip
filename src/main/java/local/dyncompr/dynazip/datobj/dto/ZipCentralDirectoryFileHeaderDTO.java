package local.dyncompr.dynazip.datobj.dto;

import java.util.Map;

/**
 * Draft - For showing an idea only...
 * @author Guillermo Fernandez
 */
public class ZipCentralDirectoryFileHeaderDTO {
	
	private int offset = -1;
	
	private int length = -1;
	
	private Map<String, Object> header = null;
	
	public ZipCentralDirectoryFileHeaderDTO() {
		// NOTHING TO DO
	}
	
	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public Map<String, Object> getHeader() {
		return header;
	}

	public void setHeader(Map<String, Object> header) {
		this.header = header;
	}
	
}
