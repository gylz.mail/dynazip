package local.dyncompr.dynazip.datobj.dto;

import java.util.Date;

/**
 * Draft - For showing an idea only...
 * @author Guillermo Fernandez
 */
public class CompDataPersistedFileDTO<T> {
	
	private String fileName = null;
	
	private T content = null;
	
	private int compressedMethod = 0;
	
	private long crc32 = -1L;
	
	private int uncompressedSize = 0;
	
	private int compressedSize = 0;
	
	private Date creationDateTime = null;
	
	private Date updateDateTime = null;
	
	private String creationProgram = null;
	
	private String updateProgram = null;
	
	public CompDataPersistedFileDTO() {
		// NOTHING TO DO
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public T getContent() {
		return content;
	}

	public void setContent(T content) {
		this.content = content;
	}

	public int getCompressedMethod() {
		return compressedMethod;
	}

	public void setCompressedMethod(int compressedMethod) {
		this.compressedMethod = compressedMethod;
	}

	public long getCrc32() {
		return crc32;
	}

	public void setCrc32(long crc32) {
		this.crc32 = crc32;
	}

	public int getUncompressedSize() {
		return uncompressedSize;
	}

	public void setUncompressedSize(int uncompressedSize) {
		this.uncompressedSize = uncompressedSize;
	}

	public int getCompressedSize() {
		return compressedSize;
	}

	public void setCompressedSize(int compressedSize) {
		this.compressedSize = compressedSize;
	}

	public Date getCreationDateTime() {
		return creationDateTime;
	}

	public void setCreationDateTime(Date creationDateTime) {
		this.creationDateTime = creationDateTime;
	}

	public Date getUpdateDateTime() {
		return updateDateTime;
	}

	public void setUpdateDateTime(Date updateDateTime) {
		this.updateDateTime = updateDateTime;
	}

	public String getCreationProgram() {
		return creationProgram;
	}

	public void setCreationProgram(String creationProgram) {
		this.creationProgram = creationProgram;
	}

	public String getUpdateProgram() {
		return updateProgram;
	}

	public void setUpdateProgram(String updateProgram) {
		this.updateProgram = updateProgram;
	}
	
}
