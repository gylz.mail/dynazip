package local.dyncompr.dynazip.datobj.svo;

import java.util.Map;

import local.dyncompr.dynazip.metadata.EndOfCentralDirectoryRecord;

/**
 * Draft - For showing an idea only...
 * @author Guillermo Fernandez
 */
public class ZipEndOfCentralDirectoryRecordSVO {
	
	private byte[] bytes = null;
	
	private int offset = -1;
	
	private int length = -1;
	
	private Map<EndOfCentralDirectoryRecord, Object> record;
	
	public ZipEndOfCentralDirectoryRecordSVO() {
		// NOTHING TO DO
	}

	public byte[] getBytes() {
		return bytes;
	}

	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public Map<EndOfCentralDirectoryRecord, Object> getRecord() {
		return record;
	}

	public void setRecord(Map<EndOfCentralDirectoryRecord, Object> record) {
		this.record = record;
	}
	
}
