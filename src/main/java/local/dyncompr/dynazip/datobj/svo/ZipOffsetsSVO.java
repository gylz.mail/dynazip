package local.dyncompr.dynazip.datobj.svo;

import java.util.List;
import java.util.Map;

import local.dyncompr.dynazip.metadata.Signature;

/**
 * Draft - For showing an idea only...
 * @author Guillermo Fernandez
 */
public class ZipOffsetsSVO {
	
	private byte[] bytes = null;
	
	private Map<Signature, List<Integer>> offsets = null;
	
	public ZipOffsetsSVO() {
		// NOTHING TO DO
	}

	public byte[] getBytes() {
		return bytes;
	}

	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}

	public Map<Signature, List<Integer>> getOffsets() {
		return offsets;
	}

	public void setOffsets(Map<Signature, List<Integer>> offsets) {
		this.offsets = offsets;
	}
	
}
