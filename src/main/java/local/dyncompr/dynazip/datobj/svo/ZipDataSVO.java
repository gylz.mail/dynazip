package local.dyncompr.dynazip.datobj.svo;

/**
 * Draft - For showing an idea only...
 * @author Guillermo Fernandez
 */
public class ZipDataSVO {
	
	private String fileName = null;
	
	private int offset = -1;
	
	private int length = -1;
	
	private String content = null;
	
	public ZipDataSVO() {
		// NOTHING TO DO
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
}
