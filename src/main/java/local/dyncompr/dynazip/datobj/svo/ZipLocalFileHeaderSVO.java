package local.dyncompr.dynazip.datobj.svo;

import java.util.Map;

import local.dyncompr.dynazip.metadata.LocalFileHeader;

/**
 * Draft - For showing an idea only...
 * @author Guillermo Fernandez
 */
public class ZipLocalFileHeaderSVO {
	
	private byte[] bytes = null;
	
	private int offset = -1;
	
	private int length = -1;
	
	private Map<LocalFileHeader, Object> header = null;
	
	public ZipLocalFileHeaderSVO() {
		// NOTHING TO DO
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public byte[] getBytes() {
		return bytes;
	}

	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}

	public Map<LocalFileHeader, Object> getHeader() {
		return header;
	}

	public void setHeader(Map<LocalFileHeader, Object> header) {
		this.header = header;
	}
	
}
