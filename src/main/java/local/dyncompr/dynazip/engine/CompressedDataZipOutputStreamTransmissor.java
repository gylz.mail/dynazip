package local.dyncompr.dynazip.engine;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;

import local.dyncompr.dynazip.metadata.CentralDirectoryFileHeader;
import local.dyncompr.dynazip.metadata.DataDescriptor;
import local.dyncompr.dynazip.metadata.EndOfCentralDirectoryRecord;
import local.dyncompr.dynazip.metadata.LocalFileHeader;
import local.dyncompr.dynazip.persistence.model.CompressedData;
import local.dyncompr.dynazip.util.ByteOps;
import local.dyncompr.dynazip.util.CompressOps;
import local.dyncompr.dynazip.util.DOSDateTimeOps;

/**
 * Draft - For showing an idea only...
 * @author Guillermo Fernandez
 */
public class CompressedDataZipOutputStreamTransmissor {
	
	private OutputStream out = null;
	
	private int[] lfhOffsets = null;
	
	private int cfhOffsetsStart = -1;
	
	private static class LFH {
		
		private static final byte[] DEFAULT_VERSION_NEEDED_TO_EXTRACT = new byte[] {20, 0};
		
		private static final byte[] DEFAULT_GENERAL_PURPOSE_BIT_FLAG = new byte[] {8, 8};
		
	}
	
	private static class CDFH {
			
		private static final byte[] DEFAULT_VERSION_MADE_BY = new byte[] {20, 0};
		
		private static final byte[] DEFAULT_VERSION_NEEDED_TO_EXTRACT = new byte[] {20, 0};
		
		private static final byte[] DEFAULT_GENERAL_PURPOSE_BIT_FLAG = new byte[] {8, 8};
		
	}
	
	public CompressedDataZipOutputStreamTransmissor(OutputStream out) {
		this.out = out;
		this.lfhOffsets = null;
		this.cfhOffsetsStart = -1;
	}
	
	public void write(List<CompressedData> datum) throws IOException {
		lfhOffsets = new int[datum.size()];
		cfhOffsetsStart = writeDatum(datum);
		writeCentralDirectory(datum);
	}
	
	private int writeDatum(List<CompressedData> datum) throws IOException {
		int result = 0;
		int i = 0;
		for(CompressedData data : datum) {
			int size = writeLocalFileHeader(data)
					   + writeDataContent(data)
					   + writeDataDescriptor(data);
			out.flush();
			lfhOffsets[i++] = result;
			result += size;
		}
		return result;
	}
	
	private int writeLocalFileHeader(CompressedData data) throws IOException {
		int result = 0;
		Charset charset = StandardCharsets.US_ASCII;
		result += ByteOps.writeBytes(out, LocalFileHeader.LOCAL_FILE_HEADER_SIGNATURE.getSignature().getValue());
		result += ByteOps.writeBytes(out, LFH.DEFAULT_VERSION_NEEDED_TO_EXTRACT);
		result += ByteOps.writeBytes(out, LFH.DEFAULT_GENERAL_PURPOSE_BIT_FLAG);
		result += ByteOps.writeLittleEnddian(out, (short)data.getCompressedMethod());
		result += ByteOps.writeBytes(out, DOSDateTimeOps.getDOSDateAndTime(data.getCreationDateTime()));
		result += ByteOps.writeBytes(out, CompressOps.crc32ToZipFileArray(0L)); // data.getCrc32()
		result += ByteOps.writeLittleEnddian(out, 0); // data.getCompressedSize()
		result += ByteOps.writeLittleEnddian(out, 0); // data.getUncompressedSize()
		result += ByteOps.writeLittleEnddian(out, (short)(data.getFileName().length())); // File name length
		result += ByteOps.writeLittleEnddian(out, (short)0); // Extra field length
		result += ByteOps.writeString(out, data.getFileName(), charset);
		return result;
	}
	
	private int writeDataContent(CompressedData data)  throws IOException {
		ByteOps.writeBytes(out, data.getContent());
		return data.getContent().length;
	}
	
	private int writeDataDescriptor(CompressedData data)  throws IOException {
		int result = 0;
		result += ByteOps.writeBytes(out, DataDescriptor.DATA_DESCRIPTOR_RECORD.getSignature().getValue());
		result += ByteOps.writeBytes(out, CompressOps.crc32ToZipFileArray(data.getCrc32()));
		result += ByteOps.writeLittleEnddian(out, data.getCompressedSize());
		result += ByteOps.writeLittleEnddian(out, data.getUncompressedSize());
		return result;
	}
	
	private int writeCentralDirectory(List<CompressedData> datum) throws IOException {
		int result = 0;
		int index = 0;
		for(CompressedData data : datum) {
			int size = writeCentralDirectoryHeader(data, index++);
			out.flush();
			result += size;
		}
		result += writeEndOfCentralDirectoryRecord(result);
		out.flush();
		return result;
	}
	
	private int writeCentralDirectoryHeader(CompressedData data, int index) throws IOException {
		int result = 0;
		Charset charset = StandardCharsets.US_ASCII;
		result += ByteOps.writeBytes(out, CentralDirectoryFileHeader.CENTRAL_FILE_HEADER_SIGNATURE.getSignature().getValue());
		result += ByteOps.writeBytes(out, CDFH.DEFAULT_VERSION_MADE_BY);
		result += ByteOps.writeBytes(out, CDFH.DEFAULT_VERSION_NEEDED_TO_EXTRACT);
		result += ByteOps.writeBytes(out, CDFH.DEFAULT_GENERAL_PURPOSE_BIT_FLAG);
		result += ByteOps.writeLittleEnddian(out, (short)data.getCompressedMethod());
		result += ByteOps.writeBytes(out, DOSDateTimeOps.getDOSDateAndTime(data.getCreationDateTime())); // TO REVIEW
		result += ByteOps.writeBytes(out, CompressOps.crc32ToZipFileArray(data.getCrc32()));
		result += ByteOps.writeLittleEnddian(out, data.getCompressedSize());
		result += ByteOps.writeLittleEnddian(out, data.getUncompressedSize());
		result += ByteOps.writeLittleEnddian(out, (short)(data.getFileName().length())); // File name length
		result += ByteOps.writeLittleEnddian(out, (short)0); // Extra field length
		result += ByteOps.writeLittleEnddian(out, (short)0); // File comment length
		result += ByteOps.writeLittleEnddian(out, (short)0); // Disk number start
		result += ByteOps.writeLittleEnddian(out, (short)0); // Internal file attributes
		result += ByteOps.writeLittleEnddian(out, 0);        // External file attributes
		result += ByteOps.writeLittleEnddian(out, lfhOffsets[index]); // relative offset of local header 
		result += ByteOps.writeString(out, data.getFileName(), charset);
		return result;
	}
	
	private int writeEndOfCentralDirectoryRecord(int centraDirectorySize) throws IOException {
		int result = 0;
		result += ByteOps.writeBytes(out, EndOfCentralDirectoryRecord.END_OF_CENTRAL_DIR_SIGNATURE.getSignature().getValue());
		result += ByteOps.writeLittleEnddian(out, (short)0); // Number of this disk
		result += ByteOps.writeLittleEnddian(out, (short)0); // Number of the disk with the start of the central directory
		result += ByteOps.writeLittleEnddian(out, (short)lfhOffsets.length); // Total number of entries in the central directory on this disk
		result += ByteOps.writeLittleEnddian(out, (short)lfhOffsets.length); // Total number of entries in the central directory
		result += ByteOps.writeLittleEnddian(out, centraDirectorySize); // Size of the central directory
		result += ByteOps.writeLittleEnddian(out, cfhOffsetsStart); // Offset of start of central directory with respect to the starting disk number
		result += ByteOps.writeLittleEnddian(out, (short)0); // .ZIP file comment length
		return result;
	}
	
}






