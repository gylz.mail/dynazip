package local.dyncompr.dynazip.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.APPLICATION_OCTET_STREAM_VALUE;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import local.dyncompr.dynazip.datobj.dto.CompDataPersistedFileDTO;
import local.dyncompr.dynazip.datobj.dto.CompDataPersistedFilesNamesDTO;
import local.dyncompr.dynazip.datobj.svo.CompDataPersistedFileSVO;
import local.dyncompr.dynazip.service.CompressedDataService;

/**
 * Draft - For showing an idea only...
 * @author Guillermo Fernandez
 */
@RestController
@RequestMapping("/v0/downloader")
public class CompressedDataDownloaderController {
	
	@Autowired
	private CompressedDataService compressedDataService;
	
	@GetMapping(path = "data/list", produces = APPLICATION_JSON_VALUE)
	public Object getPersistedFilesNames() {
		CompDataPersistedFilesNamesDTO result = new CompDataPersistedFilesNamesDTO();
		List<String> names = compressedDataService.getAllPersistedFilesNames();
		result.setNames(names);
		return result;
	}
	
	@GetMapping(path = "data/file", produces = APPLICATION_JSON_VALUE)
	public Object getPersistedFile(@RequestParam(name = "file_name", required = true) String fileName,
								   @RequestParam(name = "compressed", required = false, defaultValue = "false") boolean compressed,
								   @RequestParam(name = "hexadecimal", required = false, defaultValue = "false") boolean hexadecimal)
								   throws UnsupportedEncodingException {
		CompDataPersistedFileDTO<Object> result = new CompDataPersistedFileDTO<>();
		CompDataPersistedFileSVO compData = compressedDataService.findPersistedFile(fileName, compressed);
		result.setFileName(compData.getFileName());
		String str = null;
		byte[] content = compData.getContent();
		if(content != null) {
			str = new String(compData.getContent(), StandardCharsets.UTF_8);
			if(hexadecimal) {
				StringBuilder strBuilder = new StringBuilder();
				for(byte b : content) {
					strBuilder.append(String.format("%02X", b));
				}
				str = strBuilder.toString();
			}
		}
		
		result.setContent(str);
		
		result.setCompressedMethod(compData.getCompressedMethod());
		result.setCompressedSize(compData.getCompressedSize());
		result.setUncompressedSize(compData.getUncompressedSize());
		result.setCrc32(compData.getCrc32());
		result.setCreationDateTime(compData.getCreationDateTime());
		result.setUpdateDateTime(compData.getUpdateDateTime());
		result.setCreationProgram(compData.getCreationProgram());
		result.setUpdateProgram(compData.getUpdateProgram());
		
		return result;
	}
	
	@GetMapping(path = "data/zip", produces = APPLICATION_OCTET_STREAM_VALUE)
	public void getZip(HttpServletResponse response,
						 @RequestParam(name = "file_name", required = true) List<String> fileNames) {
		try {
			response.addHeader("Content-disposition", "attachment;filename=data.zip");
			response.setContentType(APPLICATION_OCTET_STREAM_VALUE);
			compressedDataService.findCompressedDataForFiles(response.getOutputStream(), fileNames);
			response.flushBuffer();
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
	
}
