package local.dyncompr.dynazip.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.TEXT_PLAIN_VALUE;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Draft - For showing an idea only...
 * @author Guillermo Fernandez
 */
@RestController
public class GeneralController {
	
	@GetMapping(value = "/", produces = TEXT_PLAIN_VALUE)
	public String base() {
		return "DYNAZIP";
	}
	
	@GetMapping(value = "/ping", produces = APPLICATION_JSON_VALUE)
	public String ping() {
		String hostname = "Unknown";
		try
		{
		    InetAddress addr = InetAddress.getLocalHost();
		    hostname = addr.getHostName();
		}
		catch (UnknownHostException ex)
		{
			// NOTHING TO DO
		}
		return "{\"status\": \"Up and running!!!\", \"node\": \""
				+ hostname
				+ "\", \"timestamp\": "
				+ new Date().getTime()
				+ "}";
	}

}
