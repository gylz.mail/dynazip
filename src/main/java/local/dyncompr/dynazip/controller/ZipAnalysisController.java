package local.dyncompr.dynazip.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.zip.DataFormatException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import local.dyncompr.dynazip.datobj.dto.ZipCentralDirectoryFileHeaderDTO;
import local.dyncompr.dynazip.datobj.dto.ZipDataDTO;
import local.dyncompr.dynazip.datobj.dto.ZipDataDescriptorDTO;
import local.dyncompr.dynazip.datobj.dto.ZipInfoDTO;
import local.dyncompr.dynazip.datobj.dto.ZipLocalFileHeaderDTO;
import local.dyncompr.dynazip.datobj.svo.ZipCentralDirectoryFileHeaderSVO;
import local.dyncompr.dynazip.datobj.svo.ZipDataDescriptorSVO;
import local.dyncompr.dynazip.datobj.svo.ZipDataSVO;
import local.dyncompr.dynazip.datobj.svo.ZipEndOfCentralDirectoryRecordSVO;
import local.dyncompr.dynazip.datobj.svo.ZipLocalFileHeaderSVO;
import local.dyncompr.dynazip.datobj.svo.ZipOffsetsSVO;
import local.dyncompr.dynazip.metadata.CentralDirectoryFileHeader;
import local.dyncompr.dynazip.metadata.DataDescriptor;
import local.dyncompr.dynazip.metadata.EndOfCentralDirectoryRecord;
import local.dyncompr.dynazip.metadata.LocalFileHeader;
import local.dyncompr.dynazip.metadata.Signature;
import local.dyncompr.dynazip.service.ZipAnalysisService;

/**
 * Draft - For showing an idea only...
 * @author Guillermo Fernandez
 */
@RestController
@RequestMapping("/v0/compress/analysis")
public class ZipAnalysisController {
	
	
	@Autowired
	private ZipAnalysisService zaService;
	
	@PostMapping(path = "zipinfo", produces = APPLICATION_JSON_VALUE)
	public Object zipInfo(@RequestParam("file") MultipartFile zipFile) {
		final List<ZipInfoDTO> zipInfoList = new ArrayList<>();
		Arrays.asList(zipFile).forEach(
			file -> {
				ZipInfoDTO infoDTO = new ZipInfoDTO();
				try {
					// bytes
					byte[] bytes = file.getBytes();
					infoDTO.setBytes(bytes);
					StringBuilder strBuilder = new StringBuilder();
					for(byte b : bytes) {
						strBuilder.append(String.format("%02X", b));
					}
					infoDTO.setHexPrettyBytes(strBuilder.toString());
					
					// offsets
					ZipOffsetsSVO offsetsSVO = zaService.getZipOffsets(bytes);
					Map<String, List<Integer>> offsets = new HashMap<>();
					for(Entry<Signature, List<Integer>> entry : offsetsSVO.getOffsets().entrySet()) {
						offsets.put(entry.getKey().getTag(), entry.getValue());
					}
					infoDTO.setOffsets(offsets);
					
					// end of central directory record
					int offset = -1;
					List<Integer> lstOffsets = offsetsSVO.getOffsets().get(Signature.END_OF_CENTRAL_DIR_SIGNATURE);
					for(Integer val : lstOffsets) {
						if(val > offset) {
							offset = val;
						}
					}
					ZipEndOfCentralDirectoryRecordSVO recordSVO = zaService.getEndOfCentralDirectoryRecordInfo(bytes, offset);
					Map<String, Object> record = new HashMap<>();
					for(Entry<EndOfCentralDirectoryRecord, Object> entry : recordSVO.getRecord().entrySet()) {
						record.put(entry.getKey().getTag(), entry.getValue());
					}
					infoDTO.setEndOfCentralDirectoryRecord(record);
					infoDTO.setEndOfCentralDirectoryRecordOffset(offset);
					infoDTO.setEndOfCentralDirectoryRecordLength(recordSVO.getLength());
					
					// central directory file headers
					int numberOfHeaders = (int) (recordSVO.getRecord().get(EndOfCentralDirectoryRecord.TOTAL_NUMBER_OF_ENTRIES_IN_THE_CENTRAL_DIRECTORY));
					offset = (int) (recordSVO.getRecord().get(EndOfCentralDirectoryRecord.OFFSET_OF_START_OF_CENTRAL_DIRECTORY_WITH_RESPECT_TO_THE_STARTING_DISK_NUMBER));
					List<ZipCentralDirectoryFileHeaderSVO> cdfHeaderSVOList = zaService.getAllCentralDirectoryFileHeaderInfo(bytes, offset, numberOfHeaders);
					List<ZipCentralDirectoryFileHeaderDTO> cdfHeaderDTOList = new ArrayList<>();
					for(ZipCentralDirectoryFileHeaderSVO cdfHeaderSVO : cdfHeaderSVOList) {
						ZipCentralDirectoryFileHeaderDTO cdfHeaderDTO = new ZipCentralDirectoryFileHeaderDTO();
						Map<String, Object> header = new HashMap<>();
						for(Entry<CentralDirectoryFileHeader, Object> entry : cdfHeaderSVO.getHeader().entrySet()) {
							header.put(entry.getKey().getTag(), entry.getValue());
						}
						cdfHeaderDTO.setHeader(header);
						cdfHeaderDTO.setOffset(cdfHeaderSVO.getOffset());
						cdfHeaderDTO.setLength(cdfHeaderSVO.getLength());
						cdfHeaderDTOList.add(cdfHeaderDTO);
					}
					infoDTO.setCentralDirectoryFileHeaders(cdfHeaderDTOList);
					
					// local file headers
					List<ZipLocalFileHeaderDTO> lfHeaderDTOList = new ArrayList<>();
					List<ZipLocalFileHeaderSVO> lfHeaderSVOList = new ArrayList<>();
					for(ZipCentralDirectoryFileHeaderSVO cdfHeaderSVO : cdfHeaderSVOList) {
						ZipLocalFileHeaderDTO lfHeaderDTO = new ZipLocalFileHeaderDTO();
						offset = (int)(cdfHeaderSVO.getHeader().get(CentralDirectoryFileHeader.RELATIVE_OFFSET_OF_LOCAL_HEADER));
						ZipLocalFileHeaderSVO lfHeaderSVO  = zaService.getEndLocalFileHeaderInfo(bytes, offset);
						lfHeaderSVOList.add(lfHeaderSVO);
						Map<String, Object> header = new HashMap<>();
						for(Entry<LocalFileHeader, Object> entry : lfHeaderSVO.getHeader().entrySet()) {
							header.put(entry.getKey().getTag(), entry.getValue());
						}
						lfHeaderDTO.setHeader(header);
						lfHeaderDTO.setOffset(lfHeaderSVO.getOffset());
						lfHeaderDTO.setLength(lfHeaderSVO.getLength());
						lfHeaderDTOList.add(lfHeaderDTO);
					}
					infoDTO.setLocalFileHeaders(lfHeaderDTOList);
					
					// data descriptors
					List<ZipDataDescriptorDTO> datDescriptorDTOList = new ArrayList<>();
					Iterator<ZipCentralDirectoryFileHeaderSVO> cdfHeaderIt = cdfHeaderSVOList.iterator();
					Iterator<ZipLocalFileHeaderSVO> lfHeaderIt = lfHeaderSVOList.iterator();
					while(cdfHeaderIt.hasNext() && lfHeaderIt.hasNext()) {
						ZipCentralDirectoryFileHeaderSVO cdfHeaderSVO = cdfHeaderIt.next();
						ZipLocalFileHeaderSVO lfHeaderSVO = lfHeaderIt.next();
						int cdfHeaderFlag = (int)(cdfHeaderSVO.getHeader().get(CentralDirectoryFileHeader.GENERAL_PURPOSE_BIT_FLAG));
						boolean dataDescriptorIncluded = ((cdfHeaderFlag & 0x0800) == 0x0800);
						if(dataDescriptorIncluded) {
							// TODO Check when is not compressed and when it should be detected the un/compressed
							// size in the local file header instead of the central directory file header.
							offset = (int)(cdfHeaderSVO.getHeader().get(CentralDirectoryFileHeader.COMPRESSED_SIZE))
									 + lfHeaderSVO.getOffset() + lfHeaderSVO.getLength();
							ZipDataDescriptorDTO dataDescriptorDTO = new ZipDataDescriptorDTO();
							ZipDataDescriptorSVO dataDescriptorSVO = zaService.getDataDescriptorInfo(bytes, offset);
							Map<String, Object> descriptor = new HashMap<>();
							for(Entry<DataDescriptor, Object> entry : dataDescriptorSVO.getDescriptor().entrySet()) {
								descriptor.put(entry.getKey().getTag(), entry.getValue());
							}
							dataDescriptorDTO.setDescriptor(descriptor);
							dataDescriptorDTO.setOffset(offset);
							dataDescriptorDTO.setLength(dataDescriptorSVO.getLength());
							datDescriptorDTOList.add(dataDescriptorDTO);
						}
					}
					infoDTO.setDataDescriptors(datDescriptorDTOList);
					
					// data
					List<ZipDataDTO> dataDTOList = new ArrayList<>();
					cdfHeaderIt = cdfHeaderSVOList.iterator();
					lfHeaderIt = lfHeaderSVOList.iterator();
					while(cdfHeaderIt.hasNext() && lfHeaderIt.hasNext()) {
						ZipCentralDirectoryFileHeaderSVO cdfHeaderSVO = cdfHeaderIt.next();
						ZipLocalFileHeaderSVO lfHeaderSVO = lfHeaderIt.next();
						offset = lfHeaderSVO.getOffset() + lfHeaderSVO.getLength();
						String fileName = (String)(lfHeaderSVO.getHeader().get(LocalFileHeader.FILE_NAME));
						int compressionMethod = (int)(cdfHeaderSVO.getHeader().get(CentralDirectoryFileHeader.COMPRESSION_METHOD));
						if(compressionMethod > 0) {
							// TODO Check when is not compressed and when it should be detected the un/compressed
							// size in the local file header instead of the central directory file header.
							int compressedSize = (int)(cdfHeaderSVO.getHeader().get(CentralDirectoryFileHeader.COMPRESSED_SIZE));
							int uncompressedSize = (int)(cdfHeaderSVO.getHeader().get(CentralDirectoryFileHeader.UNCOMPRESSED_SIZE));
							ZipDataSVO dataSVO = zaService.getBrewDataContent(bytes, offset, compressedSize, compressionMethod, uncompressedSize);
							ZipDataDTO dataDTO = new ZipDataDTO();
							dataDTO.setFileName(fileName);
							dataDTO.setOffset(offset);
							dataDTO.setLength(compressedSize);
							dataDTO.setContent(Arrays.copyOfRange(bytes, offset, offset + compressedSize - 1));
							dataDTO.setUncompressedContent(dataSVO.getContent());
							dataDTOList.add(dataDTO);
						}
					}
					infoDTO.setData(dataDTOList);
					
				} catch (DataFormatException | IOException e) { 
					infoDTO.setBytes(null);
					infoDTO.setHexPrettyBytes(null);
					infoDTO.setOffsets(null);
					infoDTO.setEndOfCentralDirectoryRecord(null);
					infoDTO.setEndOfCentralDirectoryRecordOffset(null);
					infoDTO.setEndOfCentralDirectoryRecordLength(null);
					infoDTO.setCentralDirectoryFileHeaders(null);
					infoDTO.setLocalFileHeaders(null);
					infoDTO.setDataDescriptors(null);
					infoDTO.setData(null);
					infoDTO.setErrorDetected(true);
					infoDTO.setDevMsg(e.getMessage());
					e.printStackTrace();
				}
				zipInfoList.add(infoDTO);
			}
		);
		return zipInfoList;
	}
	
}
