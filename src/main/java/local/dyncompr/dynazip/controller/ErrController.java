package local.dyncompr.dynazip.controller;

import static org.springframework.http.MediaType.TEXT_PLAIN_VALUE;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Draft - For showing an idea only...
 * @author Guillermo Fernandez
 */
@Controller
public class ErrController implements ErrorController {

	@Override
	@GetMapping(path="/error", produces = TEXT_PLAIN_VALUE)
	@ResponseBody
	public String getErrorPath() {
		return "ERROR";
	}

}
