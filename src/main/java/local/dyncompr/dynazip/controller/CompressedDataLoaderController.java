package local.dyncompr.dynazip.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import local.dyncompr.dynazip.service.CompressedDataService;

/**
 * Draft - For showing an idea only...
 * @author Guillermo Fernandez
 */
@RestController
@RequestMapping("/v0/loader")
public class CompressedDataLoaderController {
	
	@Autowired
	private CompressedDataService compressedDataService;
	
	@PostMapping(path = "data/uncompressed", produces = APPLICATION_JSON_VALUE)
	public Object loadUncompressedData(@RequestParam("file_name") String fileName,
						  		       @RequestParam("content") MultipartFile content) {
		String result = null;
		try {
			byte[] rawBytes = content.getBytes();
			byte[] bytes = new String(rawBytes, StandardCharsets.US_ASCII).getBytes();
			result = compressedDataService.deflateAndPersistFile(fileName, bytes);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}
	
}
