package local.dyncompr.dynazip.service;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;

import org.springframework.stereotype.Service;

import local.dyncompr.dynazip.datobj.svo.ZipCentralDirectoryFileHeaderSVO;
import local.dyncompr.dynazip.datobj.svo.ZipDataDescriptorSVO;
import local.dyncompr.dynazip.datobj.svo.ZipDataSVO;
import local.dyncompr.dynazip.datobj.svo.ZipEndOfCentralDirectoryRecordSVO;
import local.dyncompr.dynazip.datobj.svo.ZipLocalFileHeaderSVO;
import local.dyncompr.dynazip.datobj.svo.ZipOffsetsSVO;
import local.dyncompr.dynazip.metadata.CentralDirectoryFileHeader;
import local.dyncompr.dynazip.metadata.DataDescriptor;
import local.dyncompr.dynazip.metadata.EndOfCentralDirectoryRecord;
import local.dyncompr.dynazip.metadata.LocalFileHeader;
import local.dyncompr.dynazip.metadata.Signature;

/**
 * Draft - For showing an idea only...
 * @author Guillermo Fernandez
 */
@Service
public class ZipAnalysisService {
	
	public ZipOffsetsSVO getZipOffsets(byte[] bytes) {
		ZipOffsetsSVO result = new ZipOffsetsSVO();
		result.setBytes(bytes);
		EnumMap<Signature, List<Integer>> offsets = new EnumMap<>(Signature.class);
		for(Signature pattern : Signature.values()) {
			List<Integer> lstOffsets = Signature.getOffsets(pattern, bytes);
			offsets.put(pattern, lstOffsets);
		}
		result.setOffsets(offsets);
		return result;
	}
	
	public ZipEndOfCentralDirectoryRecordSVO getEndOfCentralDirectoryRecordInfo(byte[] bytes, int offset) {
		ZipEndOfCentralDirectoryRecordSVO result = new ZipEndOfCentralDirectoryRecordSVO();
		result.setBytes(bytes);
		result.setOffset(offset);
		Map<EndOfCentralDirectoryRecord, Object> record =
				EndOfCentralDirectoryRecord.getInfo(bytes, offset);
		result.setRecord(record);
		int length = EndOfCentralDirectoryRecord.getTotalLength(record);
		result.setLength(length);
		return result;
	}
	
	public ZipCentralDirectoryFileHeaderSVO getCentralDirectoryFileHeaderInfo(byte[] bytes, int offset) {
		ZipCentralDirectoryFileHeaderSVO result = new ZipCentralDirectoryFileHeaderSVO();
		result.setBytes(bytes);
		result.setOffset(offset);
		Map<CentralDirectoryFileHeader, Object> header = CentralDirectoryFileHeader.getInfo(bytes, offset);
		result.setHeader(header);
		int length = CentralDirectoryFileHeader.getTotalLength(header);
		result.setLength(length);
		return result;
	}
	
	public List<ZipCentralDirectoryFileHeaderSVO> getAllCentralDirectoryFileHeaderInfo(
			byte[] bytes, int offset, int numberOfHeaders) {
		List<ZipCentralDirectoryFileHeaderSVO> result = new ArrayList<>();
		int currentOffset = offset;
		for(int i = 0; i < numberOfHeaders; ++i) {
			ZipCentralDirectoryFileHeaderSVO headerSVO = getCentralDirectoryFileHeaderInfo(bytes, currentOffset);
			result.add(headerSVO);
			currentOffset += headerSVO.getLength();
		}
		return result;
	}
	
	public ZipLocalFileHeaderSVO getEndLocalFileHeaderInfo(byte[] bytes, int offset) {
		ZipLocalFileHeaderSVO result = new ZipLocalFileHeaderSVO();
		result.setBytes(bytes);
		result.setOffset(offset);
		Map<LocalFileHeader, Object> header = LocalFileHeader.getInfo(bytes, offset);
		result.setHeader(header);
		int length = LocalFileHeader.getTotalLength(header);
		result.setLength(length);
		return result;
	}
	
	public ZipDataDescriptorSVO getDataDescriptorInfo(byte[] bytes, int offset) {
		ZipDataDescriptorSVO result = new ZipDataDescriptorSVO();
		result.setBytes(bytes);
		result.setOffset(offset);
		Map<DataDescriptor, Object> descriptor = DataDescriptor.getInfo(bytes, offset);
		result.setDescriptor(descriptor);
		int length = DataDescriptor.getTotalLength(descriptor);
		result.setLength(length);
		return result;
	}
	
	public ZipDataSVO getBrewDataContent(byte[] bytes, int offset, int length, int compressionMethod, int uncompressedLength)
	throws DataFormatException {
		ZipDataSVO result = new ZipDataSVO();
		result.setOffset(offset);
		result.setLength(length);
		byte[] output = null;
		String content = null;
		if(compressionMethod == -1) {
			output = Arrays.copyOfRange(bytes, offset, offset + length - 1);
			content = new String(output, StandardCharsets.UTF_8);
		} else {
			byte[] input = Arrays.copyOfRange(bytes, offset, offset + length - 1);
			output = new byte[uncompressedLength];
			Inflater inflater = new Inflater(true);
			inflater.setInput(input);
			inflater.inflate(output);
			inflater.end();
			content = new String(output, StandardCharsets.UTF_8);
		}
		result.setContent(content);
		return result;
	}
	
}
