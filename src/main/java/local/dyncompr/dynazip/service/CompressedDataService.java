package local.dyncompr.dynazip.service;


import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.zip.DataFormatException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import local.dyncompr.dynazip.datobj.svo.CompDataPersistedFileSVO;
import local.dyncompr.dynazip.engine.CompressedDataZipOutputStreamTransmissor;
import local.dyncompr.dynazip.persistence.model.CompressedData;
import local.dyncompr.dynazip.persistence.repository.CompressedDataRepository;
import local.dyncompr.dynazip.util.CompressOps;

/**
 * Draft - For showing an idea only...
 * @author Guillermo Fernandez
 */
@Service
public class CompressedDataService {
	
	@Autowired
	private CompressedDataRepository compressedDataRepository;
	
	public static final int DEFLATED_METHOD = 8;
	
	public CompressedDataService() {
		// NOTHING TO DO
	}
	
	private static final String COMPUTER_IDENTITY;
	static {
		String str = null;
		try {
			str = InetAddress.getLocalHost().getHostName();
		} catch(Exception ex) {
			str = "UNKNOWN";
		}
		COMPUTER_IDENTITY = str;
	}
	
	private static final String DEFLATE_AND_PERSIST_FILE_CREATION_PROGRAM =
		CompressedDataService.class.getName() + ".deflateAndPersistFile@" + COMPUTER_IDENTITY;
	
	public String deflateAndPersistFile(String fileName, byte[] content) {
		byte[] deflatedContent = CompressOps.deflate(content);
		long crc = CompressOps.crc32(content);
		CompressedData cData = new CompressedData();
		cData.setFileName(fileName);
		cData.setUncompressedSize(content.length);
		cData.setCompressedMethod(DEFLATED_METHOD);
		cData.setContent(deflatedContent);
		cData.setCompressedSize(deflatedContent.length);
		cData.setCrc32(crc);
		cData.setCreationDateTime(new Date());
		cData.setCreationProgram(DEFLATE_AND_PERSIST_FILE_CREATION_PROGRAM);
		return compressedDataRepository.save(cData).getFileName();
	}
	
	public CompDataPersistedFileSVO findPersistedFile(String fileName, boolean compressed) {
		CompDataPersistedFileSVO result = new CompDataPersistedFileSVO();
		CompressedData cData = compressedDataRepository.findByFileName(fileName);
		if(cData != null) {
			result.setFileName(cData.getFileName());
			result.setCompressedMethod(cData.getCompressedMethod());
			result.setCompressedSize(cData.getCompressedSize());
			result.setUncompressedSize(cData.getUncompressedSize());
			result.setCrc32(cData.getCrc32());
			result.setCreationDateTime(cData.getCreationDateTime());
			result.setUpdateDateTime(cData.getUpdateDateTime());
			result.setCreationProgram(cData.getCreationProgram());
			result.setUpdateProgram(cData.getUpdateProgram());
			if(compressed) {
				result.setContent(cData.getContent());
			} else { // Inflate
				try {
					byte[] bytes = cData.getContent();
					int compressedSize = cData.getCompressedSize();
					int uncompressedSize = cData.getUncompressedSize();
					byte[] content = CompressOps.inflate(bytes, compressedSize, uncompressedSize);
					result.setContent(content);
				} catch(DataFormatException e) {
					e.printStackTrace();
				}
			}
		}
		return result;
	}
	
	public List<String> getAllPersistedFilesNames() {
		return compressedDataRepository.findAllFileNames()
									   .stream()
									   .map(CompressedData::getFileName)
									   .collect(Collectors.toList());
	}
	
	public boolean findCompressedDataForFiles(OutputStream out, List<String> fileNames)
	throws IOException {
		List<CompressedData> datum = compressedDataRepository.findByFileNameIn(fileNames);
		CompressedDataZipOutputStreamTransmissor outStream = new CompressedDataZipOutputStreamTransmissor(out); 
		outStream.write(datum);
		return true;
	}
	
}
