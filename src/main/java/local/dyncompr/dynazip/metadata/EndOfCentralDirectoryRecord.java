package local.dyncompr.dynazip.metadata;

import static local.dyncompr.dynazip.util.ByteOps.sumBytesLittleEnddian;

import java.util.EnumMap;
import java.util.Map;

/**
 * Draft - For showing an idea only...
 * @author Guillermo Fernandez
 */
public enum EndOfCentralDirectoryRecord {
	
	END_OF_CENTRAL_DIR_SIGNATURE(
		"END_OF_CENTRAL_DIR_SIGNATURE", 0, 4, Signature.END_OF_CENTRAL_DIR_SIGNATURE, Signature.class),
	
	NUMBER_OF_THIS_DISK(
		"NUMBER_OF_THIS_DISK", 4, 2, null, Integer.class),
	
	NUMBER_OF_THIS_DISK_WITH_THE_START_OF_THE_CENTRAL_DIRECTORY(
		"NUMBER_OF_THIS_DISK_WITH_THE_START_OF_THE_CENTRAL_DIRECTORY", 6, 2, null, Integer.class),
	
	TOTAL_NUMBER_OF_ENTRIES_IN_THE_CENTRAL_DIRECTORY_ON_THIS_DISK(
		"TOTAL_NUMBER_OF_ENTRIES_IN_THE_CENTRAL_DIRECTORY_ON_THIS_DISK", 8, 2, null, Integer.class),
	
	TOTAL_NUMBER_OF_ENTRIES_IN_THE_CENTRAL_DIRECTORY(
		"TOTAL_NUMBER_OF_ENTRIES_IN_THE_CENTRAL_DIRECTORY", 10, 2, null, Integer.class),
	
	SIZE_OF_THE_CENTRAL_DIRECTORY(
		"SIZE_OF_THE_CENTRAL_DIRECTORY", 12, 4, null, Integer.class),
	
	OFFSET_OF_START_OF_CENTRAL_DIRECTORY_WITH_RESPECT_TO_THE_STARTING_DISK_NUMBER(
		"OFFSET_OF_START_OF_CENTRAL_DIRECTORY_WITH_RESPECT_TO_THE_STARTING_DISK_NUMBER", 16, 4, null, Integer.class),
	
	ZIP_FILE_COMMENT_LENGTH(
		"ZIP_FILE_COMMENT_LENGTH", 20, 2, null, Integer.class),
	
	ZIP_FILE_COMMENT(
		"ZIP_FILE_COMMENT", 22, null, null, String.class);
	
	private String tag = null;
	
	private Integer offset = null;
	
	private Integer length = null;
	
	private Signature signature = null;
	
	private Class<?> type = null;
	
	private EndOfCentralDirectoryRecord(String tag,
										Integer offset,
										Integer length,
										Signature signature,
										Class<?> type) {
		this.tag = tag;
		this.offset = offset;
		this.length = length;
		this.signature = signature;
		this.type = type;
	}
	
	public String getTag() {
		return tag;
	}

	public int getOffset() {
		return offset;
	}

	public Integer getLength() {
		return length;
	}

	public Signature getSignature() {
		return signature;
	}
	
	public Class<?> getType() {
		return type;
	}
	
	public static Map<EndOfCentralDirectoryRecord, Object> getInfo(byte[] bytes, int offset) {
		Map<EndOfCentralDirectoryRecord, Object> result = new EnumMap<>(EndOfCentralDirectoryRecord.class);
		
		// END_OF_CENTRAL_DIR_SIGNATURE
		boolean signatureFound = END_OF_CENTRAL_DIR_SIGNATURE.signature.match(bytes, offset);
		if(!signatureFound) {
			throw new IllegalArgumentException("End of central signature not found at the indicated offset.");
		}
		result.put(END_OF_CENTRAL_DIR_SIGNATURE, END_OF_CENTRAL_DIR_SIGNATURE.signature.getValue());
		
		// NUMBER_OF_THIS_DISK
		int currentOffset = offset + NUMBER_OF_THIS_DISK.offset;
		int numBytes = NUMBER_OF_THIS_DISK.length;
		int value = sumBytesLittleEnddian(bytes, currentOffset, numBytes);
		result.put(NUMBER_OF_THIS_DISK, value);
		
		// NUMBER_OF_THIS_DISK_WITH_THE_START_OF_THE_CENTRAL_DIRECTORY
		currentOffset = offset + NUMBER_OF_THIS_DISK_WITH_THE_START_OF_THE_CENTRAL_DIRECTORY.offset;
		numBytes = NUMBER_OF_THIS_DISK_WITH_THE_START_OF_THE_CENTRAL_DIRECTORY.length;
		value = sumBytesLittleEnddian(bytes, currentOffset, numBytes);
		result.put(NUMBER_OF_THIS_DISK_WITH_THE_START_OF_THE_CENTRAL_DIRECTORY, value);
		
		// TOTAL_NUMBER_OF_ENTRIES_IN_THE_CENTRAL_DIRECTORY_ON_THIS_DISK
		currentOffset = offset + TOTAL_NUMBER_OF_ENTRIES_IN_THE_CENTRAL_DIRECTORY_ON_THIS_DISK.offset;
		numBytes = TOTAL_NUMBER_OF_ENTRIES_IN_THE_CENTRAL_DIRECTORY_ON_THIS_DISK.length;
		value = sumBytesLittleEnddian(bytes, currentOffset, numBytes);
		result.put(TOTAL_NUMBER_OF_ENTRIES_IN_THE_CENTRAL_DIRECTORY_ON_THIS_DISK, value);
		
		// TOTAL_NUMBER_OF_ENTRIES_IN_THE_CENTRAL_DIRECTORY
		currentOffset = offset + TOTAL_NUMBER_OF_ENTRIES_IN_THE_CENTRAL_DIRECTORY.offset;
		numBytes = TOTAL_NUMBER_OF_ENTRIES_IN_THE_CENTRAL_DIRECTORY.length;
		value = sumBytesLittleEnddian(bytes, currentOffset, numBytes);
		result.put(TOTAL_NUMBER_OF_ENTRIES_IN_THE_CENTRAL_DIRECTORY, value);
		
		// SIZE_OF_THE_CENTRAL_DIRECTORY
		currentOffset = offset + SIZE_OF_THE_CENTRAL_DIRECTORY.offset;
		numBytes = SIZE_OF_THE_CENTRAL_DIRECTORY.length;
		value = sumBytesLittleEnddian(bytes, currentOffset, numBytes);
		result.put(SIZE_OF_THE_CENTRAL_DIRECTORY, value);
		
		// OFFSET_OF_START_OF_CENTRAL_DIRECTORY_WITH_RESPECT_TO_THE_STARTING_DISK_NUMBER
		currentOffset = offset + OFFSET_OF_START_OF_CENTRAL_DIRECTORY_WITH_RESPECT_TO_THE_STARTING_DISK_NUMBER.offset;
		numBytes = OFFSET_OF_START_OF_CENTRAL_DIRECTORY_WITH_RESPECT_TO_THE_STARTING_DISK_NUMBER.length;
		value = sumBytesLittleEnddian(bytes, currentOffset, numBytes);
		result.put(OFFSET_OF_START_OF_CENTRAL_DIRECTORY_WITH_RESPECT_TO_THE_STARTING_DISK_NUMBER, value);
		
		// ZIP_FILE_COMMENT_LENGTH
		currentOffset = offset + ZIP_FILE_COMMENT_LENGTH.offset;
		numBytes = ZIP_FILE_COMMENT_LENGTH.length;
		value = sumBytesLittleEnddian(bytes, currentOffset, numBytes);
		result.put(ZIP_FILE_COMMENT_LENGTH, value);
		
		// ZIP_FILE_COMMENT
		if(value == 0) {
			result.put(ZIP_FILE_COMMENT, "");
		} else {
			currentOffset = offset + ZIP_FILE_COMMENT_LENGTH.offset;
			String comment = new String(bytes, currentOffset, value);
			result.put(ZIP_FILE_COMMENT, comment);
		}
		
		return result;
	}
	
	public static int getTotalLength(Map<EndOfCentralDirectoryRecord, Object> info) {
		int result = END_OF_CENTRAL_DIR_SIGNATURE.length
				     + NUMBER_OF_THIS_DISK.length
				     + NUMBER_OF_THIS_DISK_WITH_THE_START_OF_THE_CENTRAL_DIRECTORY.length
				     + TOTAL_NUMBER_OF_ENTRIES_IN_THE_CENTRAL_DIRECTORY_ON_THIS_DISK.length
				     + TOTAL_NUMBER_OF_ENTRIES_IN_THE_CENTRAL_DIRECTORY.length
				     + SIZE_OF_THE_CENTRAL_DIRECTORY.length
				     + OFFSET_OF_START_OF_CENTRAL_DIRECTORY_WITH_RESPECT_TO_THE_STARTING_DISK_NUMBER.length
				     + ZIP_FILE_COMMENT_LENGTH.length;
		result += (Integer) info.get(ZIP_FILE_COMMENT_LENGTH);
		return result;
	}
	
}
