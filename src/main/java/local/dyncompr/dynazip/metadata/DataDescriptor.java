package local.dyncompr.dynazip.metadata;

import static local.dyncompr.dynazip.util.ByteOps.sumBytesLittleEnddian;

import java.util.EnumMap;
import java.util.Map;

/**
 * Draft - For showing an idea only...
 * @author Guillermo Fernandez
 */
public enum DataDescriptor {
	
	DATA_DESCRIPTOR_RECORD(
		"DATA_DESCRIPTOR_RECORD", 0, 4, Signature.DATA_DESCRIPTOR_RECORD, Signature.class),
	
	CRC_32(
		"CRC_32", 0, 4, null, Signature.class),
		
	COMPRESSED_SIZE(
		"COMPRESSED_SIZE", 4, 4, null, Integer.class),
		
	UNCOMPRESSED_SIZE(
		"UNCOMPRESSED_SIZE", 8, 4, null, Integer.class);
	
	private final String tag;
	
	private final Integer offset;
	
	private final Integer length;
	
	private final Signature signature;
	
	private final Class<?> type;
	
	private DataDescriptor(String tag,
						   Integer offset,
						   Integer length,
						   Signature signature,
						   Class<?> type) {
		this.tag = tag;
		this.offset = offset;
		this.length = length;
		this.signature = signature;
		this.type = type;
	}
	
	public String getTag() {
		return tag;
	}

	public int getOffset() {
		return offset;
	}

	public Integer getLength() {
		return length;
	}

	public Signature getSignature() {
		return signature;
	}
	
	public Class<?> getType() {
		return type;
	}
	
	public static Map<DataDescriptor, Object> getInfo(byte[] bytes, int offset) {
		Map<DataDescriptor, Object> result = new EnumMap<>(DataDescriptor.class);
		
		// DATA_DESCRIPTOR_SIGNATURE... This is optional!!! Not always present.
		boolean signatureFound = DATA_DESCRIPTOR_RECORD.signature.match(bytes, offset);
		if(signatureFound) {
			result.put(DATA_DESCRIPTOR_RECORD, DATA_DESCRIPTOR_RECORD.signature.getValue());
			offset += DATA_DESCRIPTOR_RECORD.length;
		}
		
		// CRC_32
		int currentOffset = offset + CRC_32.offset;
		int numBytes = CRC_32.length;
		int value = sumBytesLittleEnddian(bytes, currentOffset, numBytes);
		result.put(CRC_32, value);
		
		// COMPRESSED_SIZE
		currentOffset = offset + COMPRESSED_SIZE.offset;
		numBytes = COMPRESSED_SIZE.length;
		value = sumBytesLittleEnddian(bytes, currentOffset, numBytes);
		result.put(COMPRESSED_SIZE, value);
		
		// UNCOMPRESSED_SIZE
		currentOffset = offset + UNCOMPRESSED_SIZE.offset;
		numBytes = UNCOMPRESSED_SIZE.length;
		value = sumBytesLittleEnddian(bytes, currentOffset, numBytes);
		result.put(UNCOMPRESSED_SIZE, value);
		
		return result;
	}
	
	public static int getTotalLength(Map<DataDescriptor, Object> info) {
		int result = CRC_32.length
			         + COMPRESSED_SIZE.length
			         + UNCOMPRESSED_SIZE.length;
		if(info.containsKey(DATA_DESCRIPTOR_RECORD)) {
			result += DATA_DESCRIPTOR_RECORD.length;
		}
		return result;
	}
	
}
