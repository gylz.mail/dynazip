package local.dyncompr.dynazip.metadata;

import static local.dyncompr.dynazip.util.ByteOps.sumBytesLittleEnddian;

import java.util.EnumMap;
import java.util.Map;

/**
 * Draft - For showing an idea only...
 * @author Guillermo Fernandez
 */
public enum LocalFileHeader {
	
	LOCAL_FILE_HEADER_SIGNATURE(
		"LOCAL_FILE_HEADER_SIGNATURE", 0, 4, Signature.LOCAL_FILE_HEADER, Signature.class),
	
	VERSION_NEEDED_TO_EXTRACT(
		"VERSION_NEEDED_TO_EXTRACT", 4, 2, null, Integer.class),
	
	GENERAL_PURPOSE_BIT_FLAG(
		"GENERAL_PURPOSE_BIT_FLAG", 6, 2, null, Integer.class),
	
	COMPRESSION_METHOD(
		"COMPRESSION_METHOD", 8, 2, null, Integer.class),
	
	LAST_MOD_FILE_TIME(
		"LAST_MOD_FILE_TIME", 10, 2, null, Integer.class),
	
	LAST_MOD_FILE_DATE(
		"LAST_MOD_FILE_DATE", 12, 2, null, Integer.class),
	
	CRC_32(
		"CRC_32", 14, 4, null, Integer.class),
	
	COMPRESSED_SIZE(
		"COMPRESSED_SIZE", 18, 4, null, Integer.class),
	
	UNCOMPRESSED_SIZE(
		"UNCOMPRESSED_SIZE", 22, 4, null, String.class),
	
	FILE_NAME_LENGTH(
		"FILE_NAME_LENGTH", 26, 2, null, String.class),
	
	EXTRA_FIELD_LENGTH(
		"EXTRA_FIELD_LENGTH", 28, 2, null, String.class),
	
	FILE_NAME(
		"FILE_NAME", 30, null, null, String.class),
	
	EXTRA_FIELD(
		"EXTRA_FIELD", null, null, null, String.class);
	
	private String tag = null;
	
	private Integer offset = null;
	
	private Integer length = null;
	
	private Signature signature = null;
	
	private Class<?> type = null;
	
	private LocalFileHeader(String tag,
										Integer offset,
										Integer length,
										Signature signature,
										Class<?> type) {
		this.tag = tag;
		this.offset = offset;
		this.length = length;
		this.signature = signature;
		this.type = type;
	}
	
	public String getTag() {
		return tag;
	}

	public int getOffset() {
		return offset;
	}

	public Integer getLength() {
		return length;
	}

	public Signature getSignature() {
		return signature;
	}
	
	public Class<?> getType() {
		return type;
	}
	
	public static Map<LocalFileHeader, Object> getInfo(byte[] bytes, int offset) {
		Map<LocalFileHeader, Object> result = new EnumMap<>(LocalFileHeader.class);
		
		// LOCAL_FILE_HEADER_SIGNATURE
		boolean signatureFound = LOCAL_FILE_HEADER_SIGNATURE.signature.match(bytes, offset);
		if(!signatureFound) {
			throw new IllegalArgumentException("Local file header signature not found at the indicated offset.");
		}
		result.put(LOCAL_FILE_HEADER_SIGNATURE, LOCAL_FILE_HEADER_SIGNATURE.signature.getValue());
		
		// VERSION_NEEDED_TO_EXTRACT
		int currentOffset = offset + VERSION_NEEDED_TO_EXTRACT.offset;
		int numBytes = VERSION_NEEDED_TO_EXTRACT.length;
		int value = sumBytesLittleEnddian(bytes, currentOffset, numBytes);
		result.put(VERSION_NEEDED_TO_EXTRACT, value);
		
		// GENERAL_PURPOSE_BIT_FLAG
		currentOffset = offset + GENERAL_PURPOSE_BIT_FLAG.offset;
		numBytes = GENERAL_PURPOSE_BIT_FLAG.length;
		value = sumBytesLittleEnddian(bytes, currentOffset, numBytes);
		result.put(GENERAL_PURPOSE_BIT_FLAG, value);
		
		// COMPRESSION_METHOD
		currentOffset = offset + COMPRESSION_METHOD.offset;
		numBytes = COMPRESSION_METHOD.length;
		value = sumBytesLittleEnddian(bytes, currentOffset, numBytes);
		result.put(COMPRESSION_METHOD, value);
		
		// LAST_MOD_FILE_TIME
		currentOffset = offset + LAST_MOD_FILE_TIME.offset;
		numBytes = LAST_MOD_FILE_TIME.length;
		value = sumBytesLittleEnddian(bytes, currentOffset, numBytes);
		result.put(LAST_MOD_FILE_TIME, value);
		
		// LAST_MOD_FILE_DATE
		currentOffset = offset + LAST_MOD_FILE_DATE.offset;
		numBytes = LAST_MOD_FILE_DATE.length;
		value = sumBytesLittleEnddian(bytes, currentOffset, numBytes);
		result.put(LAST_MOD_FILE_DATE, value);
		
		// CRC_32
		currentOffset = offset + CRC_32.offset;
		numBytes = CRC_32.length;
		value = sumBytesLittleEnddian(bytes, currentOffset, numBytes);
		result.put(CRC_32, value);
		
		// COMPRESSED_SIZE
		currentOffset = offset + COMPRESSED_SIZE.offset;
		numBytes = COMPRESSED_SIZE.length;
		value = sumBytesLittleEnddian(bytes, currentOffset, numBytes);
		result.put(COMPRESSED_SIZE, value);
		
		// UNCOMPRESSED_SIZE
		currentOffset = offset + UNCOMPRESSED_SIZE.offset;
		numBytes = UNCOMPRESSED_SIZE.length;
		value = sumBytesLittleEnddian(bytes, currentOffset, numBytes);
		result.put(UNCOMPRESSED_SIZE, value);
		
		// FILE_NAME_LENGTH
		currentOffset = offset + FILE_NAME_LENGTH.offset;
		numBytes = FILE_NAME_LENGTH.length;
		int fileNameLength = sumBytesLittleEnddian(bytes, currentOffset, numBytes);
		result.put(FILE_NAME_LENGTH, fileNameLength);
		
		// EXTRA_FIELD_LENGTH
		currentOffset = offset + EXTRA_FIELD_LENGTH.offset;
		numBytes = EXTRA_FIELD_LENGTH.length;
		int extraFieldLength = sumBytesLittleEnddian(bytes, currentOffset, numBytes);
		result.put(EXTRA_FIELD_LENGTH, extraFieldLength);
		
		// FILE_NAME
		String fileName = null;
		if(fileNameLength == 0) {
			result.put(FILE_NAME, "");
		} else {
			currentOffset = offset + FILE_NAME.offset;
			fileName = new String(bytes, currentOffset, fileNameLength);
			result.put(FILE_NAME, fileName);
		}
		
		// EXTRA_FIELD
		if(extraFieldLength == 0) {
			result.put(EXTRA_FIELD, "");
		} else {
			currentOffset = offset + FILE_NAME.offset + fileName.length();
			String extraField = new String(bytes, currentOffset, extraFieldLength);
			result.put(EXTRA_FIELD, extraField);
		}
		
		return result;
	}
	
	public static int getTotalLength(Map<LocalFileHeader, Object> info) {
		int result = LOCAL_FILE_HEADER_SIGNATURE.length
				     + VERSION_NEEDED_TO_EXTRACT.length
				     + GENERAL_PURPOSE_BIT_FLAG.length
				     + COMPRESSION_METHOD.length
				     + LAST_MOD_FILE_TIME.length
				     + LAST_MOD_FILE_DATE.length
				     + CRC_32.length
				     + COMPRESSED_SIZE.length
				     + UNCOMPRESSED_SIZE.length
				     + FILE_NAME_LENGTH.length
				     + EXTRA_FIELD_LENGTH.length;
		result += (Integer) info.get(FILE_NAME_LENGTH);
		result += (Integer) info.get(EXTRA_FIELD_LENGTH);
		return result;
	}
	
	
}
