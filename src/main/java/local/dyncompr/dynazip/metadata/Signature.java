package local.dyncompr.dynazip.metadata;

import static local.dyncompr.dynazip.util.ByteOps.fromHexToByte;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import local.dyncompr.dynazip.util.ByteOps;

/**
 * Draft - For showing an idea only...
 * @author Guillermo Fernandez
 */
public enum Signature {
	
	PK("PK", fromHexToByte("504B".toCharArray())),
	
	LOCAL_FILE_HEADER("LOCAL_FILE_HEADER", fromHexToByte("504B0304".toCharArray())),
	
	DATA_DESCRIPTOR_RECORD("DATA_DESCRIPTOR_RECORD", fromHexToByte("504B0708".toCharArray())),
	
	ARCHIVE_EXTRA_DATA_RECORD("ARCHIVE_EXTRA_DATA_RECORD", fromHexToByte("504B0608".toCharArray())),
	
	CENTRAL_FILE_HEADER_SIGNATURE("CENTRAL_FILE_HEADER_SIGNATURE", fromHexToByte("504B0102".toCharArray())),
	
	DIGITAL_SIGNATURE("DIGITAL_SIGNATURE", fromHexToByte("504B0505".toCharArray())),
	
	ZIP64_END_OF_CENTRAL_DIRECTORY_RECORD("ZIP64_END_OF_CENTRAL_DIRECTORY_RECORD", fromHexToByte("504B0606".toCharArray())),
	
	ZIP64_END_OF_CENTRAL_DIR_LOCATOR_SIGNATURE("ZIP64_END_OF_CENTRAL_DIR_LOCATOR_SIGNATURE", fromHexToByte("504B0607".toCharArray())),
	
	END_OF_CENTRAL_DIR_SIGNATURE("END_OF_CENTRAL_DIR_SIGNATURE", fromHexToByte("504B0506".toCharArray())),
	
	SPECIAL_SPANNING_MARKER("SPECIAL_SPANNING_MARKER", fromHexToByte("504B0303".toCharArray()));
	
	private final String tag;
	
	private final byte[] value;
	
	private Signature(String tag, byte[] value) {
		this.tag = tag;
		this.value = value;
	}
	
	public byte[] getValue() {
		return Arrays.copyOf(this.value, this.value.length);
	}
	
	public int getPatternLength() {
		return this.value.length;
	}
	
	public boolean match(byte[] bytes, int offset) {
		return ByteOps.match(this.value, bytes, offset);
	}
	
	public String getTag() {
		return this.tag;
	}
	
	public static List<Integer> getOffsets(Signature zipSignature, byte[] bts) {
		List<Integer> result = new ArrayList<>();
		
		final byte[] signatureValue = zipSignature.value;
		
		for(int i = 0; i < bts.length; ++i) {
			for(int k = i, j = 0; j < signatureValue.length; ++k, ++j) {
				if(bts[k] != signatureValue[j]) {
					break;
				} else if(j == signatureValue.length - 1) {
					result.add(i);
				}
			}
		}
		
		return result;
	}
	
}
