package local.dyncompr.dynazip.metadata;

import static local.dyncompr.dynazip.util.ByteOps.sumBytesLittleEnddian;

import java.util.EnumMap;
import java.util.Map;

/**
 * Draft - For showing an idea only...
 * @author Guillermo Fernandez
 */
public enum CentralDirectoryFileHeader {
	
	CENTRAL_FILE_HEADER_SIGNATURE(
		"CENTRAL_FILE_HEADER_SIGNATURE", 0, 4, Signature.CENTRAL_FILE_HEADER_SIGNATURE, Signature.class),
	
	VERSION_MADE_BY(
		"VERSION_MADE_BY", 4, 2, null, Integer.class),
	
	VERSION_NEEDED_TO_EXTRACT(
		"VERSION_NEEDED_TO_EXTRACT", 6, 2, null, Integer.class),
	
	GENERAL_PURPOSE_BIT_FLAG(
		"GENERAL_PURPOSE_BIT_FLAG", 8, 2, null, Integer.class),
	
	COMPRESSION_METHOD(
		"COMPRESSION_METHOD", 10, 2, null, Integer.class),
	
	LAST_MOD_FILE_TIME(
		"LAST_MOD_FILE_TIME", 12, 2, null, Integer.class),
	
	LAST_MOD_FILE_DATE(
		"LAST_MOD_FILE_DATE", 14, 2, null, Integer.class),
	
	CRC_32(
		"CRC_32", 16, 4, null, Integer.class),
	
	COMPRESSED_SIZE(
		"COMPRESSED_SIZE", 20, 4, null, Integer.class),
	
	UNCOMPRESSED_SIZE(
		"UNCOMPRESSED_SIZE", 24, 4, null, Integer.class),
	
	FILE_NAME_LENGTH(
		"FILE_NAME_LENGTH", 28, 2, null, Integer.class),
	
	EXTRA_FIELD_LENGTH(
		"EXTRA_FIELD_LENGTH", 30, 2, null, Integer.class),
	
	FILE_COMMENT_LENGTH(
		"FILE_COMMENT_LENGTH", 32, 2, null, Integer.class),
	
	DISK_NUMBER_START(
		"DISK_NUMBER_START", 34, 2, null, Integer.class),
	
	INTERNAL_FILE_ATTRIBUTES(
		"DISK_NUMBER_START", 36, 2, null, Integer.class),
	
	EXTERNAL_FILE_ATTRIBUTES(
		"EXTERNAL_FILE_ATTRIBUTES", 38, 4, null, Integer.class),
	
	RELATIVE_OFFSET_OF_LOCAL_HEADER(
		"RELATIVE_OFFSET_OF_LOCAL_HEADER", 42, 4, null, Integer.class),
	
	FILE_NAME(
		"FILE_NAME", 46, null, null, String.class),
	
	EXTRA_FIELD(
		"EXTRA_FIELD", null, null, null, String.class),
	
	FILE_COMMENT(
		"FILE_COMMENT", null, null, null, String.class);
	
	private final String tag;
	
	private final Integer offset;
	
	private final Integer length;
	
	private final Signature signature;
	
	private final Class<?> type;
	
	private CentralDirectoryFileHeader(String tag,
										Integer offset,
										Integer length,
										Signature signature,
										Class<?> type) {
		this.tag = tag;
		this.offset = offset;
		this.length = length;
		this.signature = signature;
		this.type = type;
	}
	
	public String getTag() {
		return tag;
	}

	public int getOffset() {
		return offset;
	}

	public Integer getLength() {
		return length;
	}

	public Signature getSignature() {
		return signature;
	}
	
	public Class<?> getType() {
		return type;
	}
	
	public static Map<CentralDirectoryFileHeader, Object> getInfo(byte[] bytes, int offset) {
		Map<CentralDirectoryFileHeader, Object> result = new EnumMap<>(CentralDirectoryFileHeader.class);
		
		// CENTRAL_FILE_HEADER_SIGNATURE
		boolean signatureFound = CENTRAL_FILE_HEADER_SIGNATURE.signature.match(bytes, offset);
		if(!signatureFound) {
			throw new IllegalArgumentException("Central file header signature not found at the indicated offset: " + offset + ".");
		}
		result.put(CENTRAL_FILE_HEADER_SIGNATURE, CENTRAL_FILE_HEADER_SIGNATURE.signature.getValue());
		
		// VERSION_MADE_BY
		int currentOffset = offset + VERSION_MADE_BY.offset;
		int numBytes = VERSION_MADE_BY.length;
		int value = sumBytesLittleEnddian(bytes, currentOffset, numBytes);
		result.put(VERSION_MADE_BY, value);
		
		// VERSION_NEEDED_TO_EXTRACT
		currentOffset = offset + VERSION_NEEDED_TO_EXTRACT.offset;
		numBytes = VERSION_NEEDED_TO_EXTRACT.length;
		value = sumBytesLittleEnddian(bytes, currentOffset, numBytes);
		result.put(VERSION_NEEDED_TO_EXTRACT, value);
		
		// GENERAL_PURPOSE_BIT_FLAG
		currentOffset = offset + GENERAL_PURPOSE_BIT_FLAG.offset;
		numBytes = GENERAL_PURPOSE_BIT_FLAG.length;
		value = sumBytesLittleEnddian(bytes, currentOffset, numBytes);
		result.put(GENERAL_PURPOSE_BIT_FLAG, value);
		
		// COMPRESSION_METHOD
		currentOffset = offset + COMPRESSION_METHOD.offset;
		numBytes = COMPRESSION_METHOD.length;
		value = sumBytesLittleEnddian(bytes, currentOffset, numBytes);
		result.put(COMPRESSION_METHOD, value);
		
		// LAST_MOD_FILE_TIME
		currentOffset = offset + LAST_MOD_FILE_TIME.offset;
		numBytes = LAST_MOD_FILE_TIME.length;
		value = sumBytesLittleEnddian(bytes, currentOffset, numBytes);
		result.put(LAST_MOD_FILE_TIME, value);
		
		// LAST_MOD_FILE_DATE
		currentOffset = offset + LAST_MOD_FILE_DATE.offset;
		numBytes = LAST_MOD_FILE_DATE.length;
		value = sumBytesLittleEnddian(bytes, currentOffset, numBytes);
		result.put(LAST_MOD_FILE_DATE, value);
		
		// CRC_32
		currentOffset = offset + CRC_32.offset;
		numBytes = CRC_32.length;
		value = sumBytesLittleEnddian(bytes, currentOffset, numBytes);
		result.put(CRC_32, value);
		
		// COMPRESSED_SIZE
		currentOffset = offset + COMPRESSED_SIZE.offset;
		numBytes = COMPRESSED_SIZE.length;
		value = sumBytesLittleEnddian(bytes, currentOffset, numBytes);
		result.put(COMPRESSED_SIZE, value);
		
		// UNCOMPRESSED_SIZE
		currentOffset = offset + UNCOMPRESSED_SIZE.offset;
		numBytes = UNCOMPRESSED_SIZE.length;
		value = sumBytesLittleEnddian(bytes, currentOffset, numBytes);
		result.put(UNCOMPRESSED_SIZE, value);
		
		// FILE_NAME_LENGTH
		currentOffset = offset + FILE_NAME_LENGTH.offset;
		numBytes = FILE_NAME_LENGTH.length;
		int fileNameLength = sumBytesLittleEnddian(bytes, currentOffset, numBytes);
		result.put(FILE_NAME_LENGTH, fileNameLength);
		
		// EXTRA_FIELD_LENGTH
		currentOffset = offset + EXTRA_FIELD_LENGTH.offset;
		numBytes = EXTRA_FIELD_LENGTH.length;
		int extraFieldLength = sumBytesLittleEnddian(bytes, currentOffset, numBytes);
		result.put(EXTRA_FIELD_LENGTH, extraFieldLength);
		
		// FILE_COMMENT_LENGTH
		currentOffset = offset + FILE_COMMENT_LENGTH.offset;
		numBytes = FILE_COMMENT_LENGTH.length;
		int fileCommentLength = sumBytesLittleEnddian(bytes, currentOffset, numBytes);
		result.put(FILE_COMMENT_LENGTH, fileCommentLength);
		
		// DISK_NUMBER_START
		currentOffset = offset + DISK_NUMBER_START.offset;
		numBytes = DISK_NUMBER_START.length;
		value = sumBytesLittleEnddian(bytes, currentOffset, numBytes);
		result.put(DISK_NUMBER_START, value);
		
		// INTERNAL_FILE_ATTRIBUTES
		currentOffset = offset + INTERNAL_FILE_ATTRIBUTES.offset;
		numBytes = INTERNAL_FILE_ATTRIBUTES.length;
		value = sumBytesLittleEnddian(bytes, currentOffset, numBytes);
		result.put(INTERNAL_FILE_ATTRIBUTES, value);
		
		// EXTERNAL_FILE_ATTRIBUTES
		currentOffset = offset + EXTERNAL_FILE_ATTRIBUTES.offset;
		numBytes = EXTERNAL_FILE_ATTRIBUTES.length;
		value = sumBytesLittleEnddian(bytes, currentOffset, numBytes);
		result.put(EXTERNAL_FILE_ATTRIBUTES, value);
		
		// RELATIVE_OFFSET_OF_LOCAL_HEADER
		currentOffset = offset + RELATIVE_OFFSET_OF_LOCAL_HEADER.offset;
		numBytes = RELATIVE_OFFSET_OF_LOCAL_HEADER.length;
		value = sumBytesLittleEnddian(bytes, currentOffset, numBytes);
		result.put(RELATIVE_OFFSET_OF_LOCAL_HEADER, value);
		
		// FILE_NAME
		if(fileNameLength == 0) {
			result.put(FILE_NAME, "");
		} else {
			currentOffset = offset + FILE_NAME.offset;
			String fileName = new String(bytes, currentOffset, fileNameLength);
			result.put(FILE_NAME, fileName);
		}
		
		// EXTRA_FIELD
		if(extraFieldLength == 0) {
			result.put(EXTRA_FIELD, "");
		} else {
			currentOffset += fileNameLength;
			String extraField = new String(bytes, currentOffset, extraFieldLength);
			result.put(EXTRA_FIELD, extraField);
		}
		
		// FILE_COMMENT_LENGTH
		if(fileCommentLength == 0) {
			result.put(FILE_COMMENT, "");
		} else {
			currentOffset += extraFieldLength;
			String fileComment = new String(bytes, currentOffset, fileCommentLength);
			result.put(FILE_COMMENT, fileComment);
		}
		
		return result;
	}
	
	public static int getTotalLength(Map<CentralDirectoryFileHeader, Object> info) {
		int result = CENTRAL_FILE_HEADER_SIGNATURE.length
				     + VERSION_MADE_BY.length
				     + VERSION_NEEDED_TO_EXTRACT.length
				     + GENERAL_PURPOSE_BIT_FLAG.length
				     + COMPRESSION_METHOD.length
				     + LAST_MOD_FILE_TIME.length
				     + LAST_MOD_FILE_DATE.length
				     + CRC_32.length
				     + COMPRESSED_SIZE.length
				     + UNCOMPRESSED_SIZE.length
				     + FILE_NAME_LENGTH.length
				     + EXTRA_FIELD_LENGTH.length
				     + FILE_COMMENT_LENGTH.length
				     + DISK_NUMBER_START.length
				     + INTERNAL_FILE_ATTRIBUTES.length
				     + EXTERNAL_FILE_ATTRIBUTES.length
				     + RELATIVE_OFFSET_OF_LOCAL_HEADER.length;
		result += (Integer) info.get(FILE_NAME_LENGTH);
		result += (Integer) info.get(EXTRA_FIELD_LENGTH);
		result += (Integer) info.get(FILE_COMMENT_LENGTH);
		return result;
	}
	
}
