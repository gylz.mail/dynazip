### PROJECT

Dynazip

### DESCRIPTION

Suggestion for a server that saves deflated files in a database and creates zips dynamically on demand with the files from the database that the user requests for. Only ASCII files are allowed.

### EXAMPLES WITH CURL

## Load the file foo1.txt
curl -X POST -F "file_name=foo1.txt" -F "content=@/path/to/file/foo1.txt" "http://localhost:9080/v0/loader/data/uncompressed"

## Load the file foo2.txt
curl -X POST -F "file_name=foo2.txt" -F "content=@/path/to/file/foo2.txt" "http://localhost:9080/v0/loader/data/uncompressed"

## Load the file fooN.txt
curl -X POST -F "file_name=fooN.txt" -F "content=@/path/to/file/fooN.txt" "http://localhost:9080/v0/loader/data/uncompressed"

## Request data list
curl -X GET "http://localhost:9080/v0/downloader/data/list"

## Request data for file foo1.txt
curl -X GET "http://localhost:9080/v0/downloader/data/file?file_name=foo1.txt&compressed=false&hexadecimal=false"

compressed and hexadecimal are optinal (default value: false in both cases)

## Request compressed zip file with file foo1.txt in it
curl -X GET --output foo.zip "http://localhost:9080/v0/downloader/data/zip?file_name=foo1.txt"

## Request compressed zip file with files foo1.txt and foo2.txt in it
curl -X GET --output foo.zip "http://localhost:9080/v0/downloader/data/zip?file_name=foo1.txt,foo2.txt"

## Request compressed zip file with files foo1.txt, foo2.txt and fooN.txt in it
curl -X GET --output foo.zip "http://localhost:9080/v0/downloader/data/zip?file_name=foo1.txt,foo2.txt,fooN.txt"









